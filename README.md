# IDFetchedDataSource

[![CI Status](http://img.shields.io/travis/Ivan Damjanović/IDFetchedDataSource.svg?style=flat)](https://travis-ci.org/Ivan Damjanović/IDFetchedDataSource)
[![Version](https://img.shields.io/cocoapods/v/IDFetchedDataSource.svg?style=flat)](http://cocoapods.org/pods/IDFetchedDataSource)
[![License](https://img.shields.io/cocoapods/l/IDFetchedDataSource.svg?style=flat)](http://cocoapods.org/pods/IDFetchedDataSource)
[![Platform](https://img.shields.io/cocoapods/p/IDFetchedDataSource.svg?style=flat)](http://cocoapods.org/pods/IDFetchedDataSource)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IDFetchedDataSource is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "IDFetchedDataSource"
```

## Author

Ivan Damjanović, ivan.damjanovic@infinum.hr

## License

IDFetchedDataSource is available under the MIT license. See the LICENSE file for more info.
