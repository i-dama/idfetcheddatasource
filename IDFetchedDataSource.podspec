Pod::Spec.new do |s|
  s.name             = "IDFetchedDataSource"
  s.version          = "0.1.1"
  s.summary          = "IDDataSource implementation using NSFetchedResultsController"
  s.description      = <<-DESC
                      IDDataSource implementation using NSFetchedResultsController.
					  NSFetchedResultsController delegate methods are forwarder to the IDDataSourceDelegate
                       DESC
  s.homepage         = "https://bitbucket.org/i-dama/idfetcheddatasource"
  s.license          = 'MIT'
  s.author           = { "Ivan Damjanović" => "ivan.damjanovic@infinum.hr" }
  s.source           = { :git => "https://bitbucket.org/i-dama/idfetcheddatasource.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/damaofficial'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

  s.frameworks = 'CoreData'
  s.dependency 'IDDataSource'
end
