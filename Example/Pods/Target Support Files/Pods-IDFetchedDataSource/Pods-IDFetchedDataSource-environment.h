
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// IDDataSource
#define COCOAPODS_POD_AVAILABLE_IDDataSource
#define COCOAPODS_VERSION_MAJOR_IDDataSource 0
#define COCOAPODS_VERSION_MINOR_IDDataSource 1
#define COCOAPODS_VERSION_PATCH_IDDataSource 0

// IDFetchedDataSource
#define COCOAPODS_POD_AVAILABLE_IDFetchedDataSource
#define COCOAPODS_VERSION_MAJOR_IDFetchedDataSource 0
#define COCOAPODS_VERSION_MINOR_IDFetchedDataSource 1
#define COCOAPODS_VERSION_PATCH_IDFetchedDataSource 0

