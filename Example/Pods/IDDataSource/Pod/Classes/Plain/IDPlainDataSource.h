//
//  IDPlainDataSource.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>
#import "IDDataSource.h"
#import "IDPlainSection.h"

//Read only data source
@interface IDPlainDataSource : NSObject <IDDataSource>

/**
 Array of IDPlainSection objects
 */
+ (instancetype)newWithSections:(NSArray *)sections;

/**
 Creates a data source with a single section containing the items
 */
+ (instancetype)newWithItems:(NSArray *)items;

@end
