//
//  IDTableViewUpdater.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>
#import "IDDataSource.h"
#import <UIKit/UIKit.h>

@protocol IDTableViewUpdaterDelegate;


/**
 Contains all of the boilerplate code for updating table views when the data source changes
 */
@interface IDTableViewUpdater : NSObject

+ (void)updateTableView:(UITableView *)tableView onChangesFromDataSource:(id<IDDataSource>)dataSource withDelegate:(id<IDTableViewUpdaterDelegate>)delegate;

@end

@protocol IDTableViewUpdaterDelegate <NSObject>

@required
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end