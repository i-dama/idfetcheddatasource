//
//  IDTableViewUpdater.m
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import "IDTableViewUpdater.h"
#import <objc/runtime.h>

static char tableViewKey;

@interface IDTableViewUpdater () <IDDataSourceDelegate>
@property (weak, nonatomic) UITableView *tableView;
@property (weak, nonatomic) id<IDTableViewUpdaterDelegate> delegate;
@end

@implementation IDTableViewUpdater

+ (void)updateTableView:(UITableView *)tableView onChangesFromDataSource:(id<IDDataSource>)dataSource withDelegate:(id<IDTableViewUpdaterDelegate>)delegate
{
    IDTableViewUpdater *updater = [IDTableViewUpdater new];
    updater.tableView = tableView;
    updater.delegate = delegate;
    dataSource.delegate = updater;
    objc_setAssociatedObject(dataSource, &tableViewKey, updater, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Data source delegate

- (void)dataSourceWillChangeContent:(id<IDDataSource>)dataSource
{
    [self.tableView beginUpdates];
}

- (void)dataSource:(id<IDDataSource>)dataSource didChangeSection:(id)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(IDDataSourceChangeType)type
{
    switch(type) {
        case IDDataSourceChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case IDDataSourceChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}

- (void)dataSource:(id<IDDataSource>)dataSource didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(IDDataSourceChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case IDDataSourceChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case IDDataSourceChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case IDDataSourceChangeUpdate:
            if (self.delegate) {
                [self.delegate configureCell:[tableView cellForRowAtIndexPath:indexPath]
                                 atIndexPath:indexPath];
            } else {
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            break;
            
        case IDDataSourceChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}

- (void)dataSourceDidChangeContent:(id<IDDataSource>)dataSource
{
    [self.tableView endUpdates];
}


@end
