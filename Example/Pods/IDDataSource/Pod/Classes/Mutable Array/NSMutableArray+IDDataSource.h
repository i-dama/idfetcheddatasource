//
//  NSMutableArray+IDDataSource.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>
#import "IDDataSource.h"

//simple category demonstrating the addition to the data source
@interface NSMutableArray (IDDataSource) <IDDataSource>

- (void)ID_addObject:(id)anObject;

@end
