//
//  NSMutableArray+IDDataSource.m
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import "NSMutableArray+IDDataSource.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

static char delegateKey;

@implementation NSMutableArray (IDDataSource)

- (void)setDelegate:(id<IDDataSourceDelegate>)delegate
{
    objc_setAssociatedObject(self, &delegateKey, delegate, OBJC_ASSOCIATION_ASSIGN);
}

- (id<IDDataSourceDelegate>)delegate
{
    return objc_getAssociatedObject(self, &delegateKey);
}

- (NSInteger)numberOfSections
{
    return 1;
}

- (id)sectionInfoForSection:(NSInteger)sectionIndex
{
    return nil;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)sectionIndex
{
    return self.count;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self objectAtIndex:indexPath.row];
}

- (NSIndexPath *)indexPathForItem:(id)item
{
    NSInteger index = [self indexOfObject:item];
    if (index == NSNotFound) {
        return nil;
    } else {
        return [NSIndexPath indexPathForRow:index inSection:0];
    }
}

#pragma mark - Override

- (void)ID_addObject:(id)anObject
{
    [self addObject:anObject];
    if ([self.delegate respondsToSelector:@selector(dataSourceWillChangeContent:)]) {
        [self.delegate dataSourceWillChangeContent:self];
    }
    if ([self.delegate respondsToSelector:@selector(dataSource:didChangeObject:atIndexPath:forChangeType:newIndexPath:)]) {
        [self.delegate dataSource:self didChangeObject:anObject atIndexPath:nil forChangeType:IDDataSourceChangeInsert newIndexPath:[NSIndexPath indexPathForRow:self.count-1 inSection:0]];
    }
    if ([self.delegate respondsToSelector:@selector(dataSourceDidChangeContent:)]) {
        [self.delegate dataSourceDidChangeContent:self];
    }
}

@end
