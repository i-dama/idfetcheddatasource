//
//  main.m
//  IDFetchedDataSource
//
//  Created by Ivan Damjanović on 05/22/2015.
//  Copyright (c) 2014 Ivan Damjanović. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IDAppDelegate class]));
    }
}
